package com.example.absensi.utils

object Const {
    val KEY_TOKEN = "token"
    val KEY_USERNAME = "username"
    val KEY_PASSWORD = "password"
}