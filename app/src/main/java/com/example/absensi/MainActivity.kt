package com.example.absensi

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.budiyev.android.codescanner.*
import com.example.absensi.geofence.GeofencingConstants
import com.example.absensi.model.*
import com.example.absensi.presenter.AllPresenter
import com.example.absensi.ui.ListActivity
import com.example.absensi.view.GeneralView
import java.util.*

class MainActivity : AppCompatActivity(), GeneralView{
    lateinit var presenter: AllPresenter
    private lateinit var codeScanner: CodeScanner
    private var cekQRCode = true
    lateinit var kdMakul : DataSemAktif

    lateinit var response: AbsenResponse

    @SuppressLint("ShowToast")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        presenter = AllPresenter(this, this)

        codeScanner = CodeScanner(this, scannerView)

        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false
        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                if (GeofencingConstants.NUM_LANDMARKS == GeofencingConstants.NUM_LANDMARKS) {
                    val startStr = 0
                    val endStr = 1
                    val substr = it.toString().subSequence(startStr, endStr)
                    val i = intent
                    val id = i.getStringExtra("idAbDos")
//                    val sts = i.getStringExtra("label")
                    Log.d("SUBSTR",substr.toString())
                    Log.d("XXXXXX", id.toString())
                    if(substr == id){
                        presenter.absen(substr)
                        Toast.makeText(this, "Berhasil Absen", Toast.LENGTH_LONG).show()
                        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibrator.vibrate(
                            VibrationEffect.createOneShot(
                                100,
                                VibrationEffect.DEFAULT_AMPLITUDE
                            )
                        )
                        val intent = Intent(this, ListActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, "QR Code salah", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(this, "Anda diluar jangkauan", Toast.LENGTH_LONG).show()
                }
            }
        }

        codeScanner.errorCallback = ErrorCallback {
            runOnUiThread {
                if (cekQRCode) {
                    cekQRCode = false

                }
                Toast.makeText(this, "Camera gagal: ${it.message}", Toast.LENGTH_LONG).show()
            }
        }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun showLoading() {

    }

    override fun error(error: Throwable) {

    }

    override fun success(response: Any) {

    }

    override fun hideLoading() {

    }
}