package com.example.absensi.view

import com.example.absensi.model.LoginResponse

interface LoginView {
    fun showLoading()
    fun error(error: Throwable)
    fun success(response: LoginResponse)
    fun hideLoading()
}