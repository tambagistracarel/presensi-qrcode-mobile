package com.example.absensi.ui

import android.Manifest
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProviders
import com.example.absensi.MainActivity
import com.example.absensi.R
import com.example.absensi.geofence.GeofenceBroadcastReceiver
import com.example.absensi.geofence.GeofenceViewModel
import com.example.absensi.geofence.GeofencingConstants
import com.example.absensi.model.LoginResponse
import com.example.absensi.presenter.AllPresenter
import com.example.absensi.presenter.LoginPresenter
import com.example.absensi.ui.LoginActivity.Companion.ACTION_GEOFENCE_EVENT
import com.example.absensi.utils.Preferences
import com.example.absensi.view.GeneralView
import com.example.absensi.view.LoginView
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices

class SplashScreenActivity : AppCompatActivity(), GeneralView {
    private lateinit var presenter: AllPresenter
    private var noAutoLogin = true
    private lateinit var viewModel: GeofenceViewModel
    private lateinit var geofencingClient: GeofencingClient

    private val geofencePendingIntent: PendingIntent by lazy {
        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        intent.action = SplashScreenActivity.ACTION_GEOFENCE_EVENT
        PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        viewModel = ViewModelProviders.of(this, SavedStateViewModelFactory(this.application, this))
            .get(GeofenceViewModel::class.java)
        geofencingClient = LocationServices.getGeofencingClient(this)
        presenter = AllPresenter(this,this)
        presenter.profile()
    }

    private fun addGeofenceForClue() {
        if (viewModel.geofenceIsActive()) return
        val currentGeofenceIndex = viewModel.nextGeofenceIndex()
        if (currentGeofenceIndex >= GeofencingConstants.NUM_LANDMARKS) {
            viewModel.geofenceActivated()
            return
        }
        val currentGeofenceData = GeofencingConstants.LANDMARK_DATA[currentGeofenceIndex]

        val geofence = Geofence.Builder()
            .setRequestId(currentGeofenceData.id)
            .setCircularRegion(
                currentGeofenceData.latLong.latitude,
                currentGeofenceData.latLong.longitude,
                GeofencingConstants.GEOFENCE_RADIUS_IN_METERS
            )
            .setExpirationDuration(Geofence.NEVER_EXPIRE)
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
            .build()

        val geofencingRequest = GeofencingRequest.Builder()
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            .addGeofence(geofence)
            .build()

        geofencingClient.removeGeofences(geofencePendingIntent).run {
            addOnCompleteListener {
                if (ActivityCompat.checkSelfPermission(
                        this@SplashScreenActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                }
                geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent).run {
                    addOnSuccessListener {
                        Toast.makeText(
                            this@SplashScreenActivity, R.string.geofences_added,
                            Toast.LENGTH_LONG
                        )
                            .show()
                        Log.e("Add Geofence", geofence.requestId)
                        viewModel.geofenceActivated()
                    }
                    addOnFailureListener {
                        Toast.makeText(
                            this@SplashScreenActivity, R.string.geofences_not_added,
                            Toast.LENGTH_LONG
                        ).show()
                        if ((it.message != null)) {
                            Log.w("TAGX", it.message!!)
                        }
                    }
                }
            }
        }
    }

    companion object {
        internal const val ACTION_GEOFENCE_EVENT =
            "SplashScreenActivity.action.ACTION_GEOFENCE_EVENT"
    }

    override fun showLoading() {
        Log.d("TAGX", "Loading")
    }

    override fun error(error: Throwable) {
        if (noAutoLogin) {
            noAutoLogin = false
            val username = Preferences.getUsername(this)
            val password = Preferences.getPassword(this)
            val strAuth = username + ":" + password
            val auth = "Basic " + Base64.encodeToString(strAuth.toByteArray(), Base64.NO_WRAP)
            presenter.getToken(auth)
            return
        }
        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun success(response: Any) {
        if (!noAutoLogin) {
            val loginResponse = response as LoginResponse
            val token = loginResponse.loginData.token
            Preferences.saveToken(this, token)
        }
        val res = response as LoginResponse
        val nama = res.loginData.namaMahasiswa
        addGeofenceForClue()
        val intent = Intent(applicationContext, ListActivity::class.java)
        intent.putExtra("message", nama)
        startActivity(intent)
        finish()
    }

    override fun hideLoading() {
        Log.d("TAGX", "Selesai")
    }
}