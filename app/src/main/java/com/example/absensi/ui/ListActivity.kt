package com.example.absensi.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.absensi.R
import com.example.absensi.model.DataSemAktif
import com.example.absensi.model.LoginData
import com.example.absensi.model.LoginResponse
import com.example.absensi.presenter.AllPresenter
import com.example.absensi.ui.dashboard.DashboardFragment
import com.example.absensi.ui.listabsen.ListAbsenFragment
import com.example.absensi.utils.Preferences
import com.example.absensi.view.GeneralView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.header_nav.*


class ListActivity : AppCompatActivity(), GeneralView {
    private lateinit var mToggle: ActionBarDrawerToggle
    private lateinit var dl: DrawerLayout
    private lateinit var nv: NavigationView
    lateinit var presenter: AllPresenter
    lateinit var mhs: LoginData
    lateinit var makul: DataSemAktif
    lateinit var binding: BindingAdapter

    @SuppressLint("RtlHardcoded")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        val i = intent
        val id = i.getStringExtra("message")
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        val headerView: View = navigationView.getHeaderView(0)
        headerView.findViewById<TextView>(R.id.tv_nm_mhs).text = id

        Log.d("TTTTTT", id.toString())
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mToggle = ActionBarDrawerToggle(
            this,
            findViewById(R.id.drawer_layout),
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        findViewById<DrawerLayout>(R.id.drawer_layout).addDrawerListener(mToggle)
        mToggle.syncState()
        nv = findViewById(R.id.nav_view)
        dl = findViewById(R.id.drawer_layout)
        nv.setNavigationItemSelectedListener { item ->
            val id = item.itemId
            dl.closeDrawer(Gravity.LEFT)
            when (id){
                R.id.dashboard -> {
                    val fragment = DashboardFragment.newInstance()
                    addFragment(fragment)
                }
                R.id.list_absen -> {
                    val fragment = ListAbsenFragment.newInstance()
                    addFragment(fragment)
                }
                R.id.logout -> {
                    Preferences.saveToken(this, "")
                    Preferences.savePassword("", this)
                    Preferences.saveUsername("", this)
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                }
            }
            true
        }
        val defaultFragment = ListAbsenFragment.newInstance()
        addFragment(defaultFragment)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return mToggle.onOptionsItemSelected(item)
    }

    override fun showLoading() {
        Log.d("TAGG","Loading")
    }

    override fun error(error: Throwable) {

    }

    override fun success(response: Any) {
        val tvNamaMhs = findViewById<TextView>(R.id.tv_nm_mhs)
        val loginResponse = response as LoginResponse
        val nmMhs = loginResponse.loginData.namaMahasiswa
        tvNamaMhs.text = nmMhs
    }

    override fun hideLoading() {
        Log.d("TAGG","Selesai")
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content, fragment, fragment.javaClass.simpleName)
            .commit()
    }
}