package com.example.absensi.ui.listabsen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.absensi.R
import com.example.absensi.model.*
import com.example.absensi.presenter.AllPresenter
import com.example.absensi.ui.listpertemuan.ListPertemuanFragment
import com.example.absensi.view.GeneralView

class ListAbsenFragment : Fragment(), GeneralView, ListMakulAdapter.Listener {

    lateinit var presenter: AllPresenter
    var makuls: MutableList<DataSemAktif> = mutableListOf()
    //    var absens: MutableList<Absen> = mutableListOf()
    lateinit var adapter: ListMakulAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_absen, container, false)

    }

    override fun onResume() {
        super.onResume()
        presenter.getAllMakul()
    }

    companion object {
        fun newInstance(): ListAbsenFragment = ListAbsenFragment()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        settingRV()

//        setClickButtonLogin()
//        val btnAbsen = requireView().findViewById<Button>(R.id.btn_absen)
//        btnAbsen.setOnClickListener {
//            val intent = Intent(activity?.applicationContext, MainActivity::class.java)
//            startActivity(intent)
//        }
    }

    private fun settingRV() {
        val rvListAbsen = requireView().findViewById<RecyclerView>(R.id.rv_list_absen)
        rvListAbsen.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(requireActivity().applicationContext, 1)
        rvListAbsen.layoutManager = layoutManager as RecyclerView.LayoutManager?
        rvListAbsen.itemAnimator = DefaultItemAnimator()
        adapter = ListMakulAdapter(makuls, this)
        rvListAbsen.adapter = adapter
        presenter = AllPresenter(this, requireActivity())
    }

    override fun showLoading() {

    }

    override fun error(error: Throwable) {

    }

    override fun success(response: Any) {
        val mMakulResponse = response as SemAktifResponse
        makuls.clear()
        makuls.addAll(mMakulResponse.dataSemAktif)
        adapter.notifyDataSetChanged()
    }

    override fun hideLoading() {

    }


    override fun onItemClick(makul: DataSemAktif) {
        val idDosSem = makul.idDosenSemester
        val fragment2 = ListPertemuanFragment.newInstance()
        val pindah = activity?.supportFragmentManager?.beginTransaction()
        pindah?.replace(R.id.content, fragment2)
        val bundle = Bundle()
        bundle.putString("KEYS", idDosSem) // Put anything what you want
        fragment2.arguments = bundle
//        pindah?.disallowAddToBackStack()
        pindah?.commit()
    }

}