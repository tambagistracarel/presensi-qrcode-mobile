package com.example.absensi.ui.listabsen

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.absensi.R
import com.example.absensi.model.Absen
import com.example.absensi.model.DataSemAktif

class ListMakulAdapter(
    private val makuls: MutableList<DataSemAktif>,
    private val listener: Listener
) : RecyclerView.Adapter<ListMakulAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMakulAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(makuls[position], listener)
    }

    interface Listener {
        fun onItemClick(makul: DataSemAktif)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvMakul: TextView = itemView.findViewById(R.id.tv_matkul)
        val tvNoMakul: TextView = itemView.findViewById(R.id.tv_no_matkul)
        val tvKelas: TextView = itemView.findViewById(R.id.tv_kelas)
        val btnAbsen: Button = itemView.findViewById(R.id.btn_list_absen)
        fun bindModel(makul: DataSemAktif, listener: Listener) {
            tvMakul.text = makul.namaMatakuliah
            tvNoMakul.text = makul.nomorMatakuliah
            tvKelas.text = makul.kelas
//            btnAbsen.text = makul.idDosenSemester
            btnAbsen.setOnClickListener {
                listener.onItemClick(makul)
            }

        }
    }

    override fun getItemCount(): Int {
        return makuls.size
    }

}