package com.example.absensi.ui.listpertemuan

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.absensi.MainActivity
import com.example.absensi.R
import com.example.absensi.model.AbsenResponse
import com.example.absensi.model.DataPertemuan
import com.example.absensi.model.ListPertemuaResponse
import com.example.absensi.presenter.AllPresenter
import com.example.absensi.view.GeneralView


class ListPertemuanFragment : Fragment(), GeneralView, ListPertemuanAdapter.Listener {
    lateinit var presenter: AllPresenter
    var perts: MutableList<DataPertemuan> = mutableListOf()
//    var absens: MutableList<Absen> = mutableListOf()
    lateinit var adapter: ListPertemuanAdapter
    fun MyFragment(){

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val strtext = arguments?.getString("KEYS")
        return inflater.inflate(R.layout.fragment_list_pertemuan, container, false)
    }

//    override fun onStart(pert: DataPertemuan) {
//        super.onStart()
//        Log.d("gsh", "jwhsjwhsjwhsw")
//
//        val all = pert.toString()
//        presenter.getAllPertemuan(all)
//    }

    override fun onResume() {
        super.onResume()
        val strtext = arguments?.getString("KEYS")
        presenter.getAllPertemuan(strtext.toString())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        settingRV()
    }

    companion object {
        fun newInstance(): ListPertemuanFragment = ListPertemuanFragment()
    }

    private fun settingRV() {
        val rvListAbsen = requireView().findViewById<RecyclerView>(R.id.rv_list_absen_pertemuan)
        rvListAbsen.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(requireActivity().applicationContext, 1)
        rvListAbsen.layoutManager = layoutManager as RecyclerView.LayoutManager?
        rvListAbsen.itemAnimator = DefaultItemAnimator()
        adapter = ListPertemuanAdapter(perts,this)
        rvListAbsen.adapter = adapter
        presenter = AllPresenter(this, requireActivity())
    }


    override fun showLoading() {
        Log.d("Tagx","Loading List Pertemuan")
    }

    override fun error(error: Throwable) {

    }

    override fun success(response: Any) {
        val mPertemuanResponse = response as ListPertemuaResponse
        perts.clear()
        perts.addAll(mPertemuanResponse.dataPertemuan)
        adapter.notifyDataSetChanged()
    }

    override fun hideLoading() {
        Log.d("Tagx","Selesai List Pertemuan")
    }

    override fun onItemClick(pert: DataPertemuan) {
        val id = pert.idAbsenDosen
//        val sts = pert.labelAbsenMahasiswa
        val i = Intent(requireActivity().baseContext, MainActivity::class.java)
        i.putExtra("idAbDos", id)
//        i.putExtra("label", sts)
        requireActivity().startActivity(i)
    }
}