package com.example.absensi.ui.listpertemuan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.absensi.R
import com.example.absensi.model.AbsenResponse
import com.example.absensi.model.DataPertemuan

class ListPertemuanAdapter(
    private val perts: MutableList<DataPertemuan>,
    private val listener: Listener
): RecyclerView.Adapter<ListPertemuanAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListPertemuanAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_absen, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(perts[position], listener)
    }

    interface Listener {
//        fun onResume(data: String)
        fun onItemClick(pert: DataPertemuan)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvNamaPertemuan: TextView = itemView.findViewById(R.id.tv_nm_pert)
        private val tvTglAjar: TextView = itemView.findViewById(R.id.tv_tgl_ajar)
        private val tvIdAbDos: TextView = itemView.findViewById(R.id.tv_id_ab_dos)
        private val btnAbsen: Button = itemView.findViewById(R.id.btn_absen)
        fun bindModel(pert: DataPertemuan, listener: Listener) {
            tvNamaPertemuan.text = pert.materiAbsenDosen
            tvTglAjar.text = pert.tanggalAbsenDosen
            tvIdAbDos.text = pert.idAbsenDosen
//            btnAbsen.text = pert.idAbsenDosen
            btnAbsen.setOnClickListener {
                listener.onItemClick(pert)
//                listener.onResume(pert)
            }
        }
    }

    override fun getItemCount(): Int {
        return perts.size
    }
}