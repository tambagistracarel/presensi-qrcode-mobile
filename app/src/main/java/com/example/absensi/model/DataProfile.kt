package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class DataProfile(
    @SerializedName("alamat_mahasiswa")
    val alamatMahasiswa: String,
    @SerializedName("jenis_kelamin")
    val jenisKelamin: String,
    @SerializedName("nama_dosen_wali")
    val namaDosenWali: String,
    @SerializedName("nama_mahasiswa")
    val namaMahasiswa: String,
    @SerializedName("nim_mahasiswa")
    val nimMahasiswa: String,
    @SerializedName("status_mahasiswa")
    val statusMahasiswa: String
)