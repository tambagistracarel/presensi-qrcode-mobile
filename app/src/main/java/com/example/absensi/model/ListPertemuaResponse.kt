package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class ListPertemuaResponse(
    @SerializedName("data")
    val dataPertemuan: List<DataPertemuan>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)

