package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("data")
    val dataProfile: DataProfile,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)