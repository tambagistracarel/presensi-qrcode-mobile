package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class DataPertemuan(
    @SerializedName("id_absen_dosen")
    val idAbsenDosen: String,
    @SerializedName("id_dosen_semester")
    val idDosenSemester: String,
    @SerializedName("is_closed")
    val isClosed: String,
    @SerializedName("label_absen_mahasiswa")
    val labelAbsenMahasiswa: String,
    @SerializedName("materi_absen_dosen")
    val materiAbsenDosen: String,
    @SerializedName("status_absen_mahasiswa")
    val statusAbsenMahasiswa: String,
    @SerializedName("tanggal_absen_dosen")
    val tanggalAbsenDosen: String
)