package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class SemAktifResponse(
    @SerializedName("data")
    val dataSemAktif: List<DataSemAktif>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)