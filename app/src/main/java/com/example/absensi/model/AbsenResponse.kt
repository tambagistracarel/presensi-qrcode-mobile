package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class AbsenResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)