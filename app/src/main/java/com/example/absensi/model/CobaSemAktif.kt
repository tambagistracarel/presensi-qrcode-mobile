package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class CobaSemAktif(
    @SerializedName("data")
    val dataCobaSemAktif: DataCobaSemAktif,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)