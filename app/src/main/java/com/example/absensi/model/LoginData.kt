package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class LoginData(
    @SerializedName("id_mahasiswa")
    val idMahasiswa: String,
    @SerializedName("is_dosen")
    val isDosen: Int,
    @SerializedName("is_lulus")
    val isLulus: String,
    @SerializedName("jenis_kelamin")
    val jenisKelamin: String,
    @SerializedName("nama_mahasiswa")
    val namaMahasiswa: String,
    @SerializedName("nim_mahasiswa")
    val nimMahasiswa: String,
    @SerializedName("token")
    val token: String
)