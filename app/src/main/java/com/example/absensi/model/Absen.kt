package com.example.absensi.model

import com.google.gson.annotations.SerializedName

data class Absen(
    @SerializedName("id_absen_dosen")
    val page: String
)
