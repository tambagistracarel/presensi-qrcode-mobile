package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("data")
    val loginData: LoginData,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)