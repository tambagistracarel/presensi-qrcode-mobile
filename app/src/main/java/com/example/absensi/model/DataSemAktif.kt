package com.example.absensi.model


import com.google.gson.annotations.SerializedName

data class DataSemAktif(
    @SerializedName("id_dosen_semester")
    val idDosenSemester: String,
    @SerializedName("id_matakuliah")
    val idMatakuliah: String,
    @SerializedName("kelas")
    val kelas: String,
    @SerializedName("nama_matakuliah")
    val namaMatakuliah: String,
    @SerializedName("nomor_matakuliah")
    val nomorMatakuliah: String
)