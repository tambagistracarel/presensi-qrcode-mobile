package com.example.absensi.geofence

import android.content.Context
import com.example.absensi.R
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.maps.model.LatLng
import java.util.concurrent.TimeUnit

fun errorMessage(context: Context, errorCode: Int): String {
    val resources = context.resources
    return when (errorCode) {
        GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE -> resources.getString(
            R.string.geofence_not_available
        )
        GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES -> resources.getString(
            R.string.geofence_too_many_geofences
        )
        GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS -> resources.getString(
            R.string.geofence_too_many_pending_intents
        )
        else -> resources.getString(R.string.unknown_geofence_error)
    }
}

data class LandmarkDataObject(val id: String, val latLong: LatLng)

internal object GeofencingConstants {

    val LANDMARK_DATA = arrayOf(
        LandmarkDataObject("Kampus",LatLng(-7.774626, 110.449022))
    )

//    Kampus = -7.774626, 110.449022
//    Rumah = -7.743245, 110.605720

    val NUM_LANDMARKS = LANDMARK_DATA.size
    const val GEOFENCE_RADIUS_IN_METERS = 200f
    const val EXTRA_GEOFENCE_INDEX = "GEOFENCE_INDEX"
}
