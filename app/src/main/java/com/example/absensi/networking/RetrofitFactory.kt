package com.example.absensi.networking

import android.content.Context
import com.example.absensi.utils.Preferences.getToken
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import java.util.prefs.Preferences

object RetrofitFactory {
//    val host = "103.28.112.108"
    val host = "192.168.137.1:9000"
//    val BASE_URL = "http://$host/skpi/api/v1/"
    val BASE_URL = "http://$host/api/v1/"
    fun createLogin(): RetrofitService {
        val builder = OkHttpClient().newBuilder()
        builder.connectTimeout(15, TimeUnit.SECONDS)
        builder.connectTimeout(15, TimeUnit.SECONDS)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        logging.level = HttpLoggingInterceptor.Level.BODY

        builder.addInterceptor(logging)
        val client = builder.build()

        val retrofit = Retrofit.Builder()
            .client(builder.build())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(client)
            .build()
        return retrofit.create(RetrofitService::class.java)
    }

    fun create(context: Context): RetrofitService {
        val token = getToken(context)
        val builder = OkHttpClient().newBuilder()
        builder.connectTimeout(15, TimeUnit.SECONDS)
        builder.connectTimeout(15, TimeUnit.SECONDS)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        logging.level = HttpLoggingInterceptor.Level.BODY
        //add token
        builder.addInterceptor(logging)

        builder.addInterceptor { chain ->
            val request = chain.request()
                .newBuilder()
                .addHeader("token", token.toString()).build() //penyisipan token secara otomatis
            chain.proceed(request)
        }

        val client = builder.build()

        val retrofit = Retrofit.Builder()
            .client(builder.build())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(client)
            .build()
        return retrofit.create(RetrofitService::class.java)
    }
}