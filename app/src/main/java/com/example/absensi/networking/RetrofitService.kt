package com.example.absensi.networking

import com.example.absensi.model.*
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface RetrofitService {
    @POST("login")
    fun login(
        @Header("Authorization") basic: String
    ): Deferred<LoginResponse>

    @GET("mahasiswa/profile")
    fun mhsProfile(): Deferred<ProfileResponse>

    @POST("mahasiswa/absen")
    fun absen(
        @Query("id_absen_dosen") id: String?
    ): Deferred<AbsenResponse>

    @GET("mahasiswa/absen")
    fun pertemuan(
        @Query("id_dosen_semester") id: String?
    ): Deferred<ListPertemuaResponse>

    @GET("mahasiswa/semester-aktif")
    fun semAktif(): Deferred<SemAktifResponse>

    @GET("mahasiswa/semester-aktif")
    fun noMakul(
        @Query("nomor_matakuliah") id: String?
    ): Deferred<SemAktifResponse>

//    @GET("mahasiswa/semester-aktif")
//    fun noMakul(): Deferred<SemAktifResponse>

}