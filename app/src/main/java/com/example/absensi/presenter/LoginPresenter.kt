package com.example.absensi.presenter

import android.util.Base64
import com.example.absensi.networking.RetrofitFactory
import com.example.absensi.view.LoginView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginPresenter(
    private val view : LoginView) {
    fun login(username: String, password: String){
        view.showLoading()
        val auth = "$username:$password"
        val credential = "Basic "+ Base64.encodeToString(auth.toByteArray(), Base64.NO_WRAP)
        val api = RetrofitFactory.createLogin()
        val request = api.login(credential)
        CoroutineScope(Dispatchers.Main).launch {
            try{
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getToken(auth: String) {
        view.showLoading()
        val api = RetrofitFactory.createLogin()
        val request = api.login(auth)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
}