package com.example.absensi.presenter

import android.content.Context
import android.util.Base64
import com.example.absensi.MainActivity
import com.example.absensi.networking.RetrofitFactory
import com.example.absensi.view.GeneralView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AllPresenter(private val view: GeneralView, private val context: Context) {
    fun profile(){
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.mhsProfile()
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getAllPertemuan(pertemuan: String){
        view.showLoading()
        val pert = pertemuan
        val api = RetrofitFactory.create(context)
        val request = api.pertemuan(pert)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getAllMakul(){
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.semAktif()
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getNoMakul(noMakul: String){
        view.showLoading()
        val no = noMakul
        val api = RetrofitFactory.create(context)
        val request = api.noMakul(no)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun login(username: String, password: String){
        view.showLoading()
        val auth = "$username:$password"
        val credential = "Basic "+ Base64.encodeToString(auth.toByteArray(), Base64.NO_WRAP)
        val api = RetrofitFactory.createLogin()
        val request = api.login(credential)
        CoroutineScope(Dispatchers.Main).launch {
            try{
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getToken(auth: String) {
        view.showLoading()
        val api = RetrofitFactory.createLogin()
        val request = api.login(auth)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun message(auth: String) {
        view.showLoading()
        val api = RetrofitFactory.createLogin()
        val request = api.login(auth)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun absen(absen: Any){
        view.showLoading()
        val sen = "$absen"
        val api = RetrofitFactory.create(context)
        val request = api.absen(sen)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception){
                view.error(e)
            }
            view.hideLoading()
        }
    }
}